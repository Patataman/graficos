#include "colors.inc"
#include "woods.inc"

camera {
	location <0, 1, -3>
	look_at <0.0, 0.5, 2>
}
light_source {
	<-2, 1, -2>
	color rgb <1, 1, 1>
}

union{

box{
	<0,0,0>, <1,1,1>
	texture {T_Wood9}
}

cone{
	<-0.3,0,0>, 0.5, <-0.3,1,0>, 0.2 
	texture {T_Wood9}
}
	rotate <-50,0,0>
}
background {
color White
}