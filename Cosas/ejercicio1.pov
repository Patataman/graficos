#include "colors.inc"

camera {
    location <0,5,-5>
    look_at <0,0,0>
    rotate <-30,0,10>
}

light_source {
    <10,10,-10>
    color rgb <1,1,1>
}

sphere {
   <0,1,0>, 1
   pigment {Green} 
}

plane {
   <0,1,0>, 0
   pigment {checker
            color Black
            color White}
}