#include "colors.inc"

camera {
    location <7,5,-10>
    look_at <0,0,0>
}

light_source {
    <1,10,-3>
    color rgb <1,1,1>
}

box {
   <0,0,0>, <2,2,2>
   pigment {Red} 
}