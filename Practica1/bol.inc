#include "colors.inc"
//#include "textures.inc"
//#include "glass.inc"

#declare bol_forma =
cone {           
   <0,0.5,0>, 1.9, <0,2.5,0>, 2
}

#declare redondeoBajo = 
sphere {
   <0,1.1,0>, 1.9
   scale <0,0.4,0>

}

#declare huecoBolInterior =
cone {           
   <0,1,0>, 1.7, <0,2.6,0>, 1.8 
}

#declare redondeoBolInterior = 
sphere {
   <0,1,0>, 1.7
   scale <0,0.4,0>
   translate <0,0.65,0>
}

#declare huecoInterior =
union {
    object{redondeoBolInterior}
    object{huecoBolInterior}
}

#declare anilloInferior =
torus {
    1,0.05 
    scale <0,2,0>
    translate <0,-0.3,0>
}

#declare bolEnPotencia =
merge {
    object{bol_forma}
    object{redondeoBajo}
    object{anilloInferior}
    //material{ texture {Yellow_Glass} }
}
   
//BOL FINAL
#declare Bol =
difference {
    object{bolEnPotencia}
    object{huecoInterior}
    texture {
        pigment{ color rgb< 1, 1, 1>*0.10 } //  color Gray10
        finish { phong 0.1 reflection 0.004 ambient 0.1 diffuse 0.1}
        normal { bumps 0.1 scale 0.01}
    }
    cutaway_textures
}       