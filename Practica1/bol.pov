#include "colors.inc"
#include "bol.inc"
//#include "textures.inc"
//#include "glass.inc"

camera {
    location <0,2,-8>
    look_at <0,1,0>
    rotate <0,0,0>
}


light_source {
    <10,5,-10>
    color rgb <1,1,1>
}

object {Bol}