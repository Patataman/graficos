#include "colors.inc"

#declare BasuraArriba =
blob {
    threshold 0.75
    sphere {<0,1.695,0.5>, 1,1}
    sphere {<0,0.5,0.5>, 1,1} 
    scale 2
}

#declare BasuraLateral1 =
blob {
    threshold 0.75
    sphere {<-1.05,0,0>, 1,0.96}
    sphere {<0,0.5,0.5>, 1.125,1}
    scale 2
}

#declare BasuraLateral2 =
blob {
    threshold 0.75
    sphere {<1.05,0,0>, 1.05, 0.95}
    sphere {<0,0.5,0.5>, 1.125,1}
    scale 2
}

#declare BasuraTrasera =
blob {
    threshold 0.78
    sphere {<0,0.1,1.695>, 1.01,1.24}
    sphere {<0,0.5,0.5>, 1,1.1}
    scale 2
}

#declare CosaRara =
union {
    object{BasuraArriba}
    object{BasuraTrasera}
    object{BasuraLateral1}
    object{BasuraLateral2}
    texture {
            pigment { color rgb<0.055,0.17,0.170>}
            finish { specular 0.2 roughness 0.0003 phong 1 phong_size 350
            }
    }
    
}