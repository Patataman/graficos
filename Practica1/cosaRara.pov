#include "colors.inc"
#include "cosaRara.inc"

camera {
    location <0,1,-10>
    look_at <0,1,0>
    rotate <10,0,0>
}


light_source {
    <10,10,-10>
    color rgb <1,1,1>
}

background { color rgb<0.6, 0.4, 0.8>  }

object{CosaRara}