#include "colors.inc"

#declare RadioPunto = 1.5;
#declare InterEspacio = 1.5;
#declare ExterEspacio = 2.5;
#declare Lado = ((InterEspacio + ExterEspacio)*2) + (RadioPunto*2*3);
#declare CircunferenciaRedondeo = sqrt((Lado*Lado + Lado*Lado)/2)*3.1/4;

/* 
    Puntos blancos.
    La posición determina el centro de la semiesfera.
    Se distinguen 3 niveles.
*/
#declare P0 = ExterEspacio+RadioPunto;
#declare P1 = ExterEspacio+RadioPunto*3+InterEspacio;
#declare P2 = ExterEspacio+RadioPunto*5+InterEspacio*2;

/*
    Cubo del dado ya redondeado
    Sus coordenadas finales son <0, 0, 0> <Lado, Lado, Lado>
*/
#declare Cubo =
difference{
    box {
        <-Lado/2, -Lado/2, -Lado/2> <Lado/2, Lado/2, Lado/2>
        
    }
    difference{
        sphere{
            <0, 0, 0>, CircunferenciaRedondeo + Lado
        }
        sphere{
            <0, 0, 0>, CircunferenciaRedondeo
        }
    }
    cutaway_textures
    translate <Lado/2, Lado/2, Lado/2>
    pigment { color rgbf < 1, 0, 0, 0.65> }
}

/*
    Construcción de las caras, la 6 es la frontal
*/
#declare Dado =
difference{
    object{Cubo}

    // 6
    sphere{
        <P0,P0, 0>, RadioPunto
        pigment { White }
    }
    sphere{
        <P2,P0, 0>, RadioPunto
        pigment { White }
    }

    sphere{
        <P0,P1, 0>, RadioPunto
        pigment { White }
    }
    sphere{
        <P2,P1, 0>, RadioPunto
        pigment { White }
    }

    sphere{
        <P0,P2, 0>, RadioPunto
        pigment { White }
    }
    sphere{
        <P2,P2, 0>, RadioPunto
        pigment { White }
    }

    // 5
    sphere{
        <0, P0, P0>, RadioPunto
        pigment { White }
    }
    sphere{
        <0, P2, P0>, RadioPunto
        pigment { White }
    }
    sphere{
        <0, P1, P1>, RadioPunto
        pigment { White }
    }
    sphere{
        <0, P0, P2>, RadioPunto
        pigment { White }
    }
    sphere{
        <0, P2, P2>, RadioPunto
        pigment { White }
    }

    // 4
    sphere{
        <P0, 0, P0>, RadioPunto
        pigment { White }
    }
    sphere{
        <P2, 0, P0>, RadioPunto
        pigment { White }
    }
    sphere{
        <P0, 0, P2>, RadioPunto
        pigment { White }
    }
    sphere{
        <P2, 0, P2>, RadioPunto
        pigment { White }
    }

    // 3
    sphere{
        <P0, Lado, P0>, RadioPunto
        pigment { White }
    }
    sphere{
        <P1, Lado, P1>, RadioPunto
        pigment { White }
    }
    sphere{
        <P2, Lado, P2>, RadioPunto
        pigment { White }
    }

    // 2
    sphere{
        <Lado, P0, P0>, RadioPunto
        pigment { White }
    }
    sphere{
        <Lado, P2, P2>, RadioPunto
        pigment { White }
    }

    // 1
    sphere{
        <P1, P1, Lado>, RadioPunto
        pigment { White }
    }

    translate <-Lado/2, -Lado/2, -Lado/2>
}