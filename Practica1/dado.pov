#include "colors.inc"
#include "dado.inc"

// Vistas

#declare Alzado =
camera {
    location <0,0,-70>
    look_at <0,0,0>
    rotate<10,0,0>
}

#declare Planta =
camera {
    location <0,30,0>
    look_at <0,0,0>
}

#declare Perfil =
camera {
    location <30,0,0>
    look_at <0,0,0>
}

#declare Random =
camera {
    location <30,30,-30>
    look_at <0,0,0>
}

camera{Alzado}


light_source {
    <10,5,-10>
    color rgb <1,1,1>
}

plane {
    <0,1,0> 0
    pigment {Green}
    
}

object{Dado translate <0,Lado,0>}