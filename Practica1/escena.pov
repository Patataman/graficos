#include "dado.inc"
#include "cosaRara.inc"
#include "vaso.inc"
#include "bol.inc"
#include "pelota.inc"
#include "colors.inc"


// Vistas
#declare Alzado =
camera {
    location <0,0,-40>
    look_at <0,0,0>
    rotate<10,0,0>
}

#declare Planta =
camera {
    location <0,50,0>
    look_at <0,0,0>
}

#declare PlantaFoto =
camera {
    location <2,50,0>
    look_at <2,0,0>
}

#declare Perfil =
camera {
    location <80,0,0>
    look_at <0,0,0>
}

#declare Random =
camera {
    location <80,80,-80>
    look_at <0,0,0>
}

#declare Entrega =
camera {
    location <0,8,-35>
    look_at <0,5,40>
    rotate<5,0,0>
}

camera{Entrega}


light_source {
    <15,100,-40>
    color rgb <1, 0.75, 0.47>
    spotlight    
}

plane {
    <0,1,0> 0
    pigment {rgb <0.9,0.9,0.9> }
    finish {
    	    specular  0.00
            roughness 1.00
            ambient   0.25
    }
    
}

plane {
    <0,90,0> 0
    pigment {rgb <0.9,0.9,0.9> }
    finish {
    	    specular  0.00
            roughness 1.00
            ambient   0.25
    }
    rotate <90,0,0>
    translate<0,0,70>
    
}

/*
	Dados
	 D E
	A B C
*/
#declare escena =
union {
    
    union{
    	// A
    	object{Dado
    		rotate <-90,0,90>
    		translate <-4,0,0>
    	}
    	// B
    	object{Dado
    		rotate <-90,-90,-90>
    		translate <Lado + 1.5,0,-2>
    	}
    	// C
    	object{Dado
    		rotate <0,90,180>
    		translate <Lado*2.1 + 5.5,0,-4>
    	}
    	// D
    	object{Dado
    		rotate <0,90,90>
    		translate <Lado/2-2,Lado+0.01,0>
    	}
    	// E
    	object{Dado
    		rotate <-90,0,90>
    		translate <Lado*3/2 + 5 ,Lado+0.01,-2>
    	}
    	
    	rotate <0,12,0>
        translate  <-70,Lado/2,-80>
        scale<0.11,0.11,0.11>
    }                    
    
    // Bol
    object{Bol
    	rotate <0,0,0>
    	translate <2.7,0.6,-4>
    	scale<2,2,2>
    }
    
    // CosaRara
    object{CosaRara
    	rotate <0,-45,0>
    	translate <-1.9,0.8,2.0>
    	scale <2.7, 2.8, 2.7>
    }
    
    // Vaso
    object{Vaso
    	rotate <0,0,0>
    	translate <0,0,0>
    	scale<8.8,11,8.8>
    }
    
    // Pelota
    object{Pelota
    	rotate <0,0,110>
    	rotate <110,0,0>
    	rotate <0,48,0>
    	rotate <0,0,49>
    	translate <2.4,1,0.5>
    	scale <5, 5.5, 5>
    }             
}

object{escena
        rotate <0,-10,0>}