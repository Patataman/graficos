#include "colors.inc"
#include "textures.inc"
#include "glass.inc"


#declare esfera =
sphere{
    <0,1,0> 10
    pigment {White}
    material {
        texture {
            pigment { image_map { png "textureBall.png" map_type 1} }
            finish { phong 0.1 reflection 0.00}
            normal { bumps 0.1 scale 0.01}
        }
    }
    scale<0.1,0.1,0.1>
}

#declare Pelota =
object{ esfera }