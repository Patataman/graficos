#include "colors.inc"
#include "textures.inc"
#include "glass.inc"
#include "pelota.inc"

camera {
    location <1,1,-5>
    look_at <0,0,0>
    rotate <0,0,0>
}           


light_source {
    <10,5,-10>
    color rgb <1,1,1>
}

background { color rgb<0, 0, 0.8> }

plane {
    <0,1,0> 0
    pigment {Green}
    
}

object {
        object{ Pelota   
                rotate<0,90,180>}
        translate<1,2,2>
}