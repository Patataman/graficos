#include "colors.inc"
#include "textures.inc"
#include "glass.inc"

   
#declare topVaso =
sor {
    8,
    <0.5,0.0>,
    <0.5,0.1>,
    <0.53, 0.4>,
    <0.58, 0.5>,   
    <0.66, 0.65>,
    <0.69, 0.75>,
    <0.71, 0.85>,
    <0.74, 1.0>
    open
    //sturm
    material {
        texture { pigment{ rgbf <0.98, 0.98, 0.98, 0.9> }
        finish {    diffuse 0.2
                    reflection 0.09
                    specular 0.8
                    roughness 0.00003
                    phong 1 phong_size 400}
        }
        interior{ ior 1.015 caustics 0.5 dispersion 1 }
    }
    scale <0,2,0>
    translate <0,-0.3,0>
}

#declare culo =
cylinder {
    <0,0,0>,<0,0.2,0>, 0.5
    texture {     pigment{ rgbf <0.98, 0.98, 0.98, 0.9> }
                  finish { diffuse 0.1 reflection 0.1  
                  specular 0.2 roughness 0.0003 phong 1 phong_size 350}
        }
        interior{ ior 0.5 caustics 0.5 }
}                


#declare Vaso =
merge {
    object{topVaso}
    object{culo}
}