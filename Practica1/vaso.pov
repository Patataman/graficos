#include "colors.inc"
#include "textures.inc"
#include "glass.inc"
#include "vaso.inc"

camera {
    location <0,0.5,-3>
    look_at <0,0.5,0>
    rotate <0,0,0>
}


light_source {
    <10,5,-10>
    color rgb <1,1,1>
}

background { color rgb<0.6, 0.4, 0.8>  }

/*plane {
    <0,1,0> 0
    pigment {Red}
    
} */
   
object{Vaso}