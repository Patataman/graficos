
#include "colors.inc"

// Vistas

#declare Alzado =
camera {
    location <0,0,-70>
    look_at <0,0,0>
    rotate<10,0,0>
}

#declare Planta =
camera {
    location <0,22.84,0>
    look_at <0,0,0>
}

#declare Perfil =
camera {
    location <30,0,0>
    look_at <0,0,0>
}

#declare Random =
camera {
    location <1,22,1>
    look_at <0,19.84,0>
}

camera{Random}

background{color rgb <199/255,122/255,135/255>}


light_source {
    <10,50,-10>
    color rgb <1,0.75,0.47>
    spotlight
}

#declare colorines = array[7];
#declare colorines[0] = color <214/255,19/255,32/255,0.6>;
#declare colorines[1] = color <176/255,19/255,32/255,0.6>;
#declare colorines[2] = color <214/255,139/255,143/255,0.6>;
#declare colorines[3] = color <161/255,33/255,43/255,0.6>;
#declare colorines[4] = color <219/255,33/255,43/255,0.6>;
#declare colorines[5] = color <219/255,69/255,90/255,0.6>;
#declare colorines[6] = color <199/255,69/255,90/255,0.6>;

#declare cochorros = 7;
#declare angle_stepsize = (360/cochorros) * 3.14 / 180;

#declare Primitiva =
union{
	cone{<0,0,0>,1,<0,20,0>,1
		texture{ pigment{ color rgb <232/255,57/255,57/255>}} // rojo rosado
	}
	cone{<0,0,0>,1.08,<0,20,0>,1.08
		texture{ pigment{ color rgb <232/255,100/255,100/255>}} // blanco rosado
	}
}

#declare Primitiva2 =
union{
    cone{<0,0,0>,1,<0,20,0>,1
        //texture{ pigment{ color rgb <0.1,0.5,0.1>}} // rojo rosado
    }
    cone{<0,0,0>,1.08,<0,20,0>,1.08
        //texture{ pigment{ color rgb <0.2,1,0.2>}} // blanco rosado
    }
}



#macro Musculo (varX, varZ, R, lv)
    #local movimiento = 1/pow(2,R*1.1)*1.4;

    #if (lv < 5)
        #if (lv = 0)
            object{Primitiva
                pigment{colorines[mod(lv,7)]}
            }
        #end
        #if (lv != 0)
            object{Primitiva2
                pigment{colorines[mod(lv,7)]}
                scale <1/(R*pow(2,lv)), 1, 1/(R*pow(2,lv))>
                translate <varX*0.9, 0.8-0.1*lv, varZ*0.9>
            }
        #end


        #local theAngle = 0;   // start value for angle
        #while ( theAngle < 2 * 3.14 ) 
        	Musculo(varX + movimiento*cos(theAngle), varZ + movimiento*sin(theAngle),R+1,lv+1)
            #declare theAngle = theAngle + angle_stepsize;
        #end
    #end
#end

Musculo(0, 0, 1,0)