#include "colors.inc"
#include "textures.inc"
#include "metals.inc"

#declare desplazamiento = 3*(sqrt(2)/2);
light_source{     
     <desplazamiento*7.5,desplazamiento*4,-100> 1

}

/*camera{
    location <desplazamiento*7.5,desplazamiento*4,-18>
    look_at <desplazamiento*7.5,desplazamiento*4,0>
    right 1
    angle 90
    sky y

}*/ 

camera{
    location <0,0,-1.84>
    look_at <0,0/4,0>
    right 1
    angle 90
    sky y

}
#declare MAX = 12; //max 12
#declare filt = 0;

plane{
    <0,0,1> .5
    pigment {color rgb <1,1,1>}
} 

#declare semilla = seed(0);
#declare colores = array[7];
#declare colores[0] = Red;
#declare colores[1] = Orange;
#declare colores[2] = Yellow;
#declare colores[3] = Green;
#declare colores[4] = Blue;
#declare colores[5] = color <9/255,31/255,146/255>;
#declare colores[6] = color <125/255,33/255,129/255>;;
                      

#macro recursion(X, Y, V, dir)  //dir := x=0;-x=1;y=2;-y=3
    #local scala = 1/pow(2,V);
    #local scala_1 = 1/pow(2,V+1);
    #local movimiento = (scala+scala_1)*(sqrt(2)/2);
    #if(V <= MAX)
        #if(V=1)    
            sphere{
                <0,0,0> 1
                scale scala
                translate <X, Y, 0>
                pigment{colores[mod(V-1,7)]}
            }
        #end
        #if(dir != 0)
            sphere{
                <0,0,0> 1
                scale scala_1
                translate < X+movimiento, Y+movimiento, 0>
                pigment{colores[mod(V,7)]}
            }
            recursion(X+movimiento, Y+movimiento, V+1, 3)
        #end
        #if(dir != 1)                        
            sphere{
                <0,0,0> 1
                scale scala_1
                translate <X-movimiento, Y+movimiento, 0>
                pigment{colores[mod(V,7)]}
            }
            recursion(X-movimiento, Y+movimiento, V+1, 2)                              
        #end           
        #if(dir != 2)
            sphere{
                <0,0,0> 1
                scale scala_1
                translate < X+movimiento, Y-movimiento, 0>
                pigment{colores[mod(V,7)]}
            }
            recursion(X+movimiento, Y-movimiento, V+1, 1)
        #end
        #if(dir != 3)
            sphere{
                <0,0,0> 1
                scale scala_1
                translate < X-movimiento, Y-movimiento, 0>
                pigment{colores[mod(V,7)]}
            }
            recursion(X-movimiento, Y-movimiento, V+1, 0)
        #end          
    #end

#end 
#declare ii = 0;
#declare jj = 0;
/*#for(ii, 0, 8, 1)
    #for(jj, 0, 15, 1)
        recursion(jj*desplazamiento,ii*desplazamiento,1,-1)  
    #end
#end*/ 

recursion(0,0,1,-1)


