
#include "colors.inc"

// Vistas

#declare Alzado =
camera {
    location <0,0,-70>
    look_at <0,0,0>
    rotate<10,0,0>
}

#declare Planta =
camera {
    //location <0,23.7,0>
    location <0,22.84,0>
    look_at <0,0,0>
}

#declare Perfil =
camera {
    location <30,0,0>
    look_at <0,0,0>
}

#declare Random =
camera {
    location <1,22,1>
    look_at <0,19.84,0>
}

camera{Random}

background{color rgb <199/255,122/255,135/255>}


light_source {
    <10,50,-10>
    color rgb <1,0.75,0.47>
    spotlight
}

#declare colorines = array[16];
#declare colorines[0] = color <232/255,57/255,57/255,0.6>;
#declare colorines[1] = color <232/255,100/255,100/255,0.6>;
#declare colorines[2] = color <214/255,19/255,32/255,0.6>;
#declare colorines[3] = color <214/255,19*2/255,32*2/255,0.6>;
#declare colorines[4] = color <176/255,19/255,32/255,0.6>;
#declare colorines[5] = color <176/255,19*2/255,32*2/255,0.6>;
#declare colorines[6] = color <214/255,139/255,143/255,0.6>;
#declare colorines[7] = color <214/255,139*2/255,143*2/255,0.6>;
#declare colorines[8] = color <161/255,33/255,43/255,0.6>;
#declare colorines[9] = color <161/255,33*2/255,43*2/255,0.6>;
#declare colorines[10] = color <219/255,33/255,43/255,0.6>;
#declare colorines[11] = color <219/255,33*2/255,43*2/255,0.6>;
#declare colorines[12] = color <219/255,69/255,90/255,0.6>;
#declare colorines[13] = color <219/255,69*2/255,90*2/255,0.6>;
#declare colorines[14] = color <199/255,69/255,90/255,0.6>;
#declare colorines[15] = color <199/255,69*2/255,90*2/255,0.6>;

#declare cochorros = 7;
#declare angle_stepsize = (360/cochorros) * 3.14 / 180;

#macro Primitiva (uno,dos)
union{
	cone{<0,0,0>,1,<0,20,0>,1
		texture{ pigment{ colorines[mod(uno,7)]}} // rojo rosado
	}
	cone{<0,0,0>,1.08,<0,20,0>,1.08
		texture{ pigment{ colorines[mod(dos,7)]}} // blanco rosado
	}
}
#end



#macro Musculo (varX, varZ, R, lv)
    #local movimiento = 1/pow(2,R*1.1)*1.4;

     #if (lv < 5)
    union{
    #if (lv = 0)
        object{Primitiva (lv,lv+1)}
        }
    #end
    #if (lv != 0)
        object{Primitiva (lv,lv+1)
        scale <1/(R*pow(2,lv)), 1, 1/(R*pow(2,lv))>
        translate <varX*0.9, 0.8-0.1*lv, varZ*0.9>
        }
    #end


        #local theAngle = 0;   // start value for angle
        #while ( theAngle < 2 * 3.14 ) 
        	//Exterior
        	Musculo(varX + movimiento*cos(theAngle), varZ + movimiento*sin(theAngle),R+1,lv+1)
            #declare theAngle = theAngle + angle_stepsize;
        #end
    }
    #end

#end

Musculo(0, 0, 1,0)
/*object{Primitiva}
object{Primitiva
            translate < 2 * cos(angle_stepsize*2), 0.2, 2*sin(angle_stepsize*2)>
            scale <0.5,0,0.5>}*/

//backup
/*
#macro Musculo (varX, varZ, R, lv)
     #if (lv < 4)
union{
        object{Primitiva
            translate <varX, 0, varZ>
            scale <R, 1, R>}
        #local theAngle = 0;   // start value for angle
        #while ( theAngle < 2 * 3.14 ) 
            //Exterior
            Musculo(varX+R*4/5,R/5,lv+1)  
            translate <R * cos(theAngle), 0 ,R * sin(theAngle)>} // al sitio
            // medio
            object {Musculo(varX+R*2/5,R/5, lv+1)  
            translate <0,(R * cos(theAngle+(angle_stepsize/2)), 0, R * sin(angle+(angle_stepsize/2))>)} //al sitio
            #declare theAngle = theAngle + angle_stepsize;
        #end
        object{Musculo(0,R/5,lv+1)}
}
     #end

#end
*/

  /*
#declare Nr = 0;   // start value for Nr
#while ( Nr < 30 ) // as long as the value Nr is smaller then 30,
	//do the following commands:  
	object{Primitiva}
	rotate<0,Nr * 360/30 ,0>
	texture{ pigment{ color 
	rgb
	<1,0.65,0>}
	finish { 
	phong
	1.0} }
	#declare Nr = Nr + 1;     // increase the value of Nr by 1
#end // this draws a full circle of 30 spheres (Nr.0 to Nr.29)!!!!
                                                                    
       */                                                             
//http://www.losersjuegos.com.ar/referencia/articulos/seno_coseno

       /*
int x, y;
    float angle_stepsize = 0.1;
 
    // go through all angles from 0 to 2 * PI radians
    while (angle < 2 * PI)    {
        x = R * cos (angle)
        y = R * sin (angle)
 
        angle += angle_stepsize
    }*/