
#include "colors.inc"

// Vistas

#declare Alzado =
camera {
    location <0,0,-70>
    look_at <0,0,0>
    rotate<10,0,0>
}

#declare Planta =
camera {
    //location <0,23.7,0>
    location <0,22.84,0>
    look_at <0,0,0>
}

#declare Perfil =
camera {
    location <30,0,0>
    look_at <0,0,0>
}

#declare Random =
camera {
    location <1,22,1>
    look_at <0,19.84,0>
}

camera{Random}


light_source {
    <10,5,-10>
    color rgb <1,1,1>
}

/*plane {
    <0,1,0> 0
    pigment {Green}
    
}*/

#declare cochorros = 4;
#declare angle_stepsize = (360/cochorros) * 3.14 / 180;

#declare Primitiva =
union{
	cone{<0,0,0>,1,<0,20,0>,1
		texture{ pigment{ color rgb <232/255,57/255,57/255>}} // rojo rosado
	}
	cone{<0,0,0>,1.08,<0,20,0>,1.08
		texture{ pigment{ color rgb <232/255,100/255,100/255>}} // blanco rosado
	}
}

#declare Primitiva2 =
union{
    cone{<0,0,0>,1,<0,20,0>,1
        texture{ pigment{ color rgb <0.1,0.5,0.1>}} // rojo rosado
    }
    cone{<0,0,0>,1.08,<0,20,0>,1.08
        texture{ pigment{ color rgb <0.2,1,0.2>}} // blanco rosado
    }
}



#macro Musculo (varX, varZ, R, lv)
    #local movimiento = 1/pow(2,R*1.1)*1.8;

     #if (lv < 4)
    union{
    #if (lv = 0)
        object{Primitiva
        }
    #end
    #if (lv != 0)
        object{Primitiva2
        scale <1/(R*pow(1.4,lv)), 1, 1/(R*pow(1.4,lv))>
        translate <varX*0.7, 0.1*lv, varZ*0.7>
        }
    #end


        #local theAngle = 0;   // start value for angle
        #while ( theAngle < 2 * 3.14 ) 
        	//Exterior
        	Musculo(varX + movimiento*cos(theAngle), varZ + movimiento*sin(theAngle),R+1,lv+1)
            #declare theAngle = theAngle + angle_stepsize;
        #end
    }
    #end

#end

Musculo(0, 0, 1,0)
/*object{Primitiva}
object{Primitiva
            translate < 2 * cos(angle_stepsize*2), 0.2, 2*sin(angle_stepsize*2)>
            scale <0.5,0,0.5>}*/

//backup
/*
#macro Musculo (varX, varZ, R, lv)
     #if (lv < 4)
union{
        object{Primitiva
            translate <varX, 0, varZ>
            scale <R, 1, R>}
        #local theAngle = 0;   // start value for angle
        #while ( theAngle < 2 * 3.14 ) 
            //Exterior
            Musculo(varX+R*4/5,R/5,lv+1)  
            translate <R * cos(theAngle), 0 ,R * sin(theAngle)>} // al sitio
            // medio
            object {Musculo(varX+R*2/5,R/5, lv+1)  
            translate <0,(R * cos(theAngle+(angle_stepsize/2)), 0, R * sin(angle+(angle_stepsize/2))>)} //al sitio
            #declare theAngle = theAngle + angle_stepsize;
        #end
        object{Musculo(0,R/5,lv+1)}
}
     #end

#end
*/

  /*
#declare Nr = 0;   // start value for Nr
#while ( Nr < 30 ) // as long as the value Nr is smaller then 30,
	//do the following commands:  
	object{Primitiva}
	rotate<0,Nr * 360/30 ,0>
	texture{ pigment{ color 
	rgb
	<1,0.65,0>}
	finish { 
	phong
	1.0} }
	#declare Nr = Nr + 1;     // increase the value of Nr by 1
#end // this draws a full circle of 30 spheres (Nr.0 to Nr.29)!!!!
                                                                    
       */                                                             
//http://www.losersjuegos.com.ar/referencia/articulos/seno_coseno

       /*
int x, y;
    float angle_stepsize = 0.1;
 
    // go through all angles from 0 to 2 * PI radians
    while (angle < 2 * PI)    {
        x = R * cos (angle)
        y = R * sin (angle)
 
        angle += angle_stepsize
    }*/