#include "colors.inc"
#include "math.inc"
   
                      
/*************************************************************************/                      

//background { color Cyan }

camera {
  location <5, 3, 1>  
  //location <0, 5, 0>
  look_at <0, 0, 0>
}  
  
  
light_source { <2, 4, -3> color White}           
  
 /*****************************************************************************/
  
  // sun ---------------------------------------------------------------
light_source{ <1500,2500,-2500> color  rgb<1,1,1> }
// sky ---------------------------------------------------------------
plane{<0,1,0>,1 hollow
       texture{
        pigment{ bozo turbulence 0.92
          color_map {
           [0.00 rgb <0.2, 0.3, 1>*0.5]
           [0.50 rgb <0.2, 0.3, 1>*0.8]
           [0.70 rgb <1,1,1>]
           [0.85 rgb <0.25,0.25,0.25>]
           [1.0 rgb <0.5,0.5,0.5>]}
          scale<1,1,1.5>*2.5
          translate<1.0,0,-1>
          }// end of pigment
        finish {ambient 1 diffuse 0}
        }// end of texture
        scale 10000
     }// end of plane
  
//lake -------------------------------------------------------------------------- 
  
  plane{<0,1.1,0>, 0
      texture{pigment{rgb <.2,.2,.2>}
              finish {ambient 0.15
                      diffuse 0.55
                      brilliance 6.0
                      phong 0.8
                      phong_size 120
                      reflection 0.6} 
                      normal{ bumps 0.03
                        scale <1,0.25,0.25>*1
                        turbulence 0.6
                      }
              }// end of texture
     }// end of plane   
     
     
     //water -------------------------------------------------------------------
     #local mat =
      texture {
         pigment {rgbt <0.8,0.8,0.0,0.3>}
         finish {
            reflection { 0.1, 1.0 fresnel on }
            conserve_energy
         }
      }     
 
/*****************************************************************************/
//con mas de 3 iteraciones el ordenador se cuelga    
#declare iteraciones = 3; //numero de iteraciones a ejecutar 
#declare esferas = 20;    // numero de toroides en cada circulo interior
                

/*****************************************************************************/ 
#macro coss(contador) cos(radians(contador* 360/esferas)) #end   //coseno 
#macro sinn(contador) sin(radians(contador* 360/esferas)) #end   //seno


/*****************************************************************************/

#declare ring = torus{1, 0.02  texture {/*pigment { color Yellow }*/   mat }} //objeto toroide basico
#declare ondas = union{
   object { ring }
   object { ring scale pow(0.6, 1)}
   object { ring scale pow(0.6, 2)}
   object { ring scale pow(0.6, 3)}
   object { ring scale pow(0.6, 4)}
   object { ring scale pow(0.6, 5)}

} 
#declare figura = ondas;  //primer toroide que se va a crear 
#declare ite = 0;      //iteracion actual
#declare escala = 3;  //valor de la escala                 
#declare esA = 0.06; //escala para el primer circulo
#declare esB = 0.022;//escala para el segundo circulo
#declare esC = 0.007;
#declare angA = 1.59/2;//valor del radio para el primer circulo
#declare angB = 0.57/2;//valor del radio para el segundo circulo 
#declare angC = 0.2/2;

#while(ite < iteraciones) 
    #declare ite = ite + 1;
    #declare ii = 0;
    #declare figura = union{  
       object {ondas scale escala}//circulo exterior
       #for(ii, 0, esferas, 1)   
           object {figura scale escala * esA translate <escala * angA*coss(ii),0,escala * angA*sinn(ii)>}   //primer circulo interior
           object {figura scale escala * esB translate <escala * angB*coss(ii),0,escala * angB*sinn(ii)>}   //segundo circulo interior
           object {figura scale escala * esC translate <escala * angC*coss(ii),0,escala * angC*sinn(ii)>}   //tercer circulo interior

       #end  
       
       
    
    }        
    
#end  

object{figura}